<?php

namespace Drupal\migrate_wp_postprocess\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * Class FirstForm.
 */
class PostprocessForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'postprocess_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['your_pass'] = [
      '#type' => 'textfield',
      '#title' => t('Your Name:'),
      '#required' => TRUE,
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->hasValue('your_pass')) {
      $pass = $form_state->getValue('your_pass');
      if (!preg_match('/Gung_Wang_OPIN/', $pass)) {
        $form_state->setErrorByName('your_pass', t('Password is NOT right.'));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      if ($key == 'your_pass') {
        $value = "************";
      }
      drupal_set_message($key . ' : ' . $value);
    }
    // drupal_set_message("test !");.
    $this->migratePostpress();

  }

  /**
   * Migrate.
   */
  protected function migratePostpress() {

    $sql1 = "SELECT n_data.nid AS nid, u_data.uid AS uid, u_name.field_display_name_value AS title,
          n_data.type
          FROM
          node_field_data n_data
          INNER JOIN users_field_data u_data ON n_data.uid = u_data.uid
          INNER JOIN user__field_display_name u_name ON u_data.uid = u_name.entity_id
          WHERE n_data.status = 1 and  n_data.type='story'
          ORDER BY nid ASC
          limit 20000";

    $connection = \Drupal::database();
    $query = $connection->query($sql1);
    $result = $query->fetchAll();
    // Print "<pre>"; print_r($result); print "<pre>";.
    $i = 0;

    foreach ($result as $key => $obj) {
      $title = $obj->title;
      $nid = $obj->nid;
      // $type = $obj->type;
      // Query database to get the nid/authorId
      $sql2 = "select nid from node_field_data where title = :title and type='author' limit 1";
      $sql2 = $connection->select('node_field_data', 'n');
      // $sql2->condition('n.type', 'author', '=');
      $sql2->condition('n.title', $title);
      $sql2->addField('n', 'nid');
      $sql2->range(0, 1);
      $result2 = $sql2->execute();
      // Print "<pre>"; print_r($result2); print "<pre>";.
      $authorId = $result2->fetchField();
      // Print "<pre>Select: "; print_r($authorId); print "<pre>";.
      if ($authorId <= 0) {
        $nodeAuthor = Node::create([
          'type' => 'author',
          'title' => $title,
        ]);
        $nodeAuthor->save();
        $authorId = $nodeAuthor->id();
      }
      // Update the story content type: node__field_author.
      $nodeStory = Node::load($nid);
      // print_r($nodeStory);
      $nodeStory->field_author = $authorId;
      if ($nodeStory->save()) {
        print "$i: $authorId, $nid, $title \n";
        // drupal_set_message("$nid : $authorId : name: $title");.
        $i++;
      }
    }
    exit();

  }

}
