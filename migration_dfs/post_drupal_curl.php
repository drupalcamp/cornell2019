<?php

/**
 * @file
 * Read json files and import to Drupal via REST POST.
 * @param string $argv[1] : HOST name
 * @param string $argv[2] : Number in directory json-1, json-2, json-3
 * @param string $argv[3] : password
 * @param string $argv[4] : X-CSRF-Token
 * 
 * Author: Gung Wang
 * Date: September 2018
 * Update: September 2019
 */
require_once './settings.php';

$time_start = microtime(true);

// Run php -dsafe_mode=Off post_drupal_curl.php nydfs2.local 2 password csrf-token.
if (!isset($argc)) {
  die("argc and argv disabled\n");
}
if(!isset($argv[4])) {
  $token = $settings['token'];
}
else {
  $token = $argv[4];
}
$localDIR = $settings['outdir'] . $argv[2];

if (stristr($argv[1], '.local')) {
  $postURL = "http://" . $argv[1] . "/node?_format=json";
}
else {
  $postURL = "https://" . $argv[1] . "/node?_format=json";
}
$list_jsons = scandir($localDIR);
foreach ($list_jsons as $json) {
  if ($json == '.' || $json == '..') {
    continue;
  }
  $jsonStr = file_get_contents($localDIR . "/$json");
  if (trim($jsonStr) == "") {
    continue;
  }
  $jsonStr = str_replace("'", "&quot;", $jsonStr);

  print $localDIR . "/$json\n";
  // post_json_drupal_no_waiting($jsonStr, $postURL, $settings['username'], $settings['password'], $token);
    post_json_drupal($jsonStr, $postURL, $settings['username'], $settings['password'], $token);

}

print "\n#### Total Jsons: " . count($list_jsons) . "\n";

// Print running time.
$time_end = microtime(true);
$execution_time = ($time_end - $time_start)/60;
print "\n--------- ---------
\nTotal Execution Time: $execution_time Minuts\n";
/**
 * Execute the curl command line to post json to Drupal.
 * @param  string  $json [encoded json string]
 * @return boolean
 * php -dsafe_mode=Off post_drupal_curl.php dfs.ny.gov 2 > /Sites/migration/print.txt
 */

function post_json_drupal($json, $postURL, $strUser, $passwd, $strToken) {
  $execStr = <<<EOF
  curl --insecure \
   --include \
   --request POST \
   --user $strUser:$passwd \
   --header 'Content-type: application/json' \
   --header 'X-CSRF-Token: <$strToken>' \
   $postURL \
   --data-binary '$json'
EOF;

   // print $execStr;
  if (exec($execStr)) {
    // print "\n###\n";
    return TRUE;
  }
  else {
    print "!!!!!\n $execStr \n ";
    return FALSE;
  }
}


function post_json_drupal_no_waiting($json, $postURL, $strUser, $passwd, $strToken)
{
  if (stristr($postURL, '.local')) {
    $strUser = 'wang';
  } else {
    $strUser = 'wang';
  }
// Prepare new cURL resource
  $ch = curl_init($postURL);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLINFO_HEADER_OUT, true);
  curl_setopt($ch, CURLOPT_POST, true);
  curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
  curl_setopt($ch, CURLOPT_COOKIESESSION, true);
  curl_setopt($ch, CURLOPT_USERPWD, "$strUser:$passwd");
  // Set HTTP Header for POST request 
  $headers[] = 'Content-Type: application/json';
  $headers[] = 'X-CSRF-Token:' . $strToken;

  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
 
// Submit the POST request
  if (curl_exec($ch) === false) {
    echo 'Curl error: ' . curl_error($ch);
  } else {
    echo '* ';
  }
// Close cURL session handle
  curl_close($ch);
}
/*

curl --insecure \
--include \
--request POST \
--user wang:gungwang2019 \
--header 'Content-type: application/json' \
--header 'X-CSRF-Token: <w_23hZ-Cyhk-dF44ofLf9ntddkLHAE_5WSZfBv7Rg-I>' \
http://nydfs2.local/node?_format=json \
--data-binary '{"type":[{"target_id":"dfs_page"}],"status":[{"value":true}],"title":[{"value":"2NYSDFS Industry Letters - June 4, 2001: Foreign Branches and Agencies Establishing Operating Subsidiaries - Guidance Letter"}],"path":[{"alias":"\/legal\/industry\/il010604.htm"}],"body":[{"value":"<div class=\"pub\">\t\r\n\t\t  <h1>Industry Letters<\/h1>\r\n\t\t  <div id=\"quarter\">\r\n\t\t    <h3> General Industry Letters<\/h3>\r\n\t\t    <ul>\r\n              <li><a href=\"\/legal\/industry\/il.htm\">2018<\/a><\/li>\r\n              <li><a href=\"\/legal\/industry\/il2017.htm\">2017<\/a><\/li>\r\n              <li><a href=\"\/legal\/industry\/il2016.htm\">2016<\/a><\/li>\r\n              <li><a href=\"\/legal\/industry\/il2015.htm\">2015<\/a><\/li>\r\n              <li><a href=\"\/legal\/industry\/il2014.htm\">2014<\/a><\/li>\r\n              <li><a href=\"\/legal\/industry\/il2013.htm\">2013<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/il2012.htm\">2012<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/il2011.htm\">2011<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/il2010.htm\">2010<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/il2009.htm\"> 2009<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/il2008.htm\">2008<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/il2007.htm\">2007<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/il2006.htm\">2006<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/il2001_2005.htm\">2005 - 2001<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/il1996_2000.htm\">2000 - 1996<\/a><\/li>\r\n\t        <\/ul>\r\n\t\t    <h3>  Mortgage Banking Letters<\/h3>\r\n\t\t    <ul>\r\n              <li><a href=\"\/legal\/industry\/ilmb.htm\">2015<\/a><\/li>\r\n              <li><a href=\"\/legal\/industry\/ilmb2014.htm\">2014<\/a><\/li>\r\n              <li><a href=\"\/legal\/industry\/ilmb2013.htm\">2013<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/ilmb2012.htm\">2012<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/ilmb2011.htm\">2011<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/ilmb2010.htm\">2010<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/ilmb2009.htm\">2009<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/ilmb2008.htm\">2008<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/ilmb2007.htm\">2007<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/ilmb2006.htm\"> 2006<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/ilmb2001_2005.htm\">2005 - 2001<\/a><\/li>\r\n\t\t      <li><a href=\"\/legal\/industry\/ilmb1996_2000.htm\">2000 - 1996<\/a><\/li>\r\n\t        <\/ul>\r\n\t      <\/div>\r\n\t\t  <div id=\"threeqtr\">\r\n\t\t  <!-- InstanceBeginEditable name=\"Letters\" -->\r\n<p align=\"right\" class=\"heading2\">Foreign Branches and Agencies Establishing Operating Subsidiaries -\r\n  Guidance Letter<\/p>\r\n<hr size=\"1\">\r\n<p>June 4, 2001<\/p>\r\n<p>TO EACH INSTITUTION ADDRESSED:<\/p>\r\n<p>This letter provides guidance to those\r\n  foreign bank branches and agencies licensed by the New York State Banking\r\n  Department (the \"Department\") that may wish to establish one or\r\n  more subsidiaries to conduct business in which the branch or agency itself\r\n  is authorized to engage.<\/p>\r\n<p>The Department recognizes that various\r\n  legal, business or tax reasons often make it desirable for foreign banking\r\n  organizations (\"FBOs\") to conduct certain activities or hold\r\n  investments through a separate legal vehicle. In some instances, a FBO may\r\n  want its New York-licensed branch or agency to own a subsidiary. For\r\n  example, some asset-securitization transactions may involve formation of a\r\n  special purpose vehicle by the New York branch or agency. Similarly, legal\r\n  restrictions, such as the \"securities push-out\" provisions of\r\n  the Federal Gramm-Leach-Bliley Act, may make it necessary or desirable to\r\n  conduct certain activities in a separate company held as a subsidiary of\r\n  the New York branch or agency.<\/p>\r\n<p>The Department is committed to maintaining\r\n  a regulatory environment that offers FBOs flexibility to structure their\r\n  businesses to achieve efficiency and functionality. To that end, the\r\n  Department herein expresses its formal view that New York-licensed foreign\r\n  branches and agencies have the authority to establish subsidiaries, and\r\n  this letter provides guidance pertaining to the establishment of such\r\n  operating subsidiaries by FBOs.<\/p>\r\n<p>In general, FBOs seeking to acquire,\r\n  establish, or make an additional investment in a branch or agency\r\n  subsidiary (whether such subsidiary is a corporation, or other type of\r\n  legal entity such as a limited liability company), are advised to follow\r\n  the procedures applicable to the establishment of operating subsidiaries\r\n  by New York banks and trust companies set forth in Part 14.3 of the\r\n  General Regulations of the Banking Board. Part 14.3(a) provides prior\r\n  notice procedures for certain subsidiary investments exceeding the lesser\r\n  of one percent of a bank\u0092s or trust company\u0092s capital stock, surplus\r\n  fund and undivided profits, or five million dollars. Since a foreign bank\u0092s\r\n  worldwide capital stock, surplus fund and undivided profits do not provide\r\n  a comparable measure of the proportionality of the investment in the\r\n  subsidiary to the totality of the foreign bank\u0092s New York assets, FBOs\r\n  are advised to follow the procedures in Part 14.3(a) when the investment\r\n  in a subsidiary will exceed the lesser of one-tenth of one percent (0.1%)\r\n  of New York assets or five million dollars.<\/p>\r\n<p>FBOs may use the after-the-fact notice\r\n  procedures in Part 14.3(c) if the requirements of that section are met.\r\n  For purposes of meeting the requirement in Part 14.3(c)(1), a branch or\r\n  agency would need to meet the definition of \"well capitalized\"\r\n  that the Federal Reserve Board (\"FRB\") uses when authorizing an\r\n  extended examination cycle for branches and agencies (see 12 CFR\r\n  211.26(c)(2)(i)(C)). This requirement is that a FBO\u0092s most recently\r\n  reported capital adequacy position consists of, or is equivalent to, Tier\r\n  1 and total risk-based capital ratios of at least 6 percent and 10\r\n  percent, respectively, on a consolidated basis; or the branch or agency\r\n  has maintained on a daily basis, over the past three quarters, eligible\r\n  assets in an amount not less than 108 percent of the preceding quarter\u0092s\r\n  average third party liabilities, and sufficient liquidity currently\r\n  available to meet obligations to third parties.<\/p>\r\n<p>For purposes of Part 14.3(c)(2), a\r\n  comparable examination rating for a foreign branch or agency will be a\r\n  composite Risk Management, Operational Controls, Compliance, and Asset\r\n  Quality (ROCA) rating of at least \"1\" or \"2\" at its\r\n  most recent examination, or in the case of a branch or agency that has not\r\n  yet been examined, the branch or agency has managerial resources that the\r\n  Department otherwise determines are satisfactory.<\/p>\r\n<p>The Department will apply other relevant\r\n  regulatory standards to branches and agencies that maintain operating\r\n  subsidiaries in light of the differences in corporate structures between\r\n  New York-chartered banks and trust companies and foreign branches or\r\n  agencies. Generally speaking, a foreign branch or agency and its\r\n  subsidiary or subsidiaries will be viewed as a consolidated entity when\r\n  such consolidation is necessary to calculate a limitation or otherwise\r\n  give effect to the intent of a statute or regulation, including, for\r\n  example, for purposes of calculating asset pledge or asset maintenance\r\n  requirements, or branch or agency assessments.<\/p>\r\n<p>While the Department will permit foreign\r\n  branches and agencies to invest in subsidiaries, FBOs should consult with\r\n  the FRB concerning their proposed investments in such subsidiaries. The\r\n  FRB, under the International Banking Act, has general supervisory\r\n  authority over the operations of FBOs in the United States, including the\r\n  investments booked in FBO branches and agencies in the U.S. It is the FRB\u0092s\r\n  position that a branch or agency of a FBO has no existence separate and\r\n  apart from the FBO itself. Thus, any investment by a branch or agency is a\r\n  direct investment by the FBO and as such is subject to the Federal Bank\r\n  Holding Company Act. Therefore, FBOs are advised to assure themselves that\r\n  any investment in a subsidiary by a New York-licensed branch or agency is\r\n  permissible under the FRB\u0092s regulations governing FBO investments, and\r\n  complies with any approval or notice requirements of the FRB under, for\r\n  example, the Bank Holding Company Act or the FRB\u0092s Regulation K. The\r\n  Department intends to consult with the FRB concerning such investments.<\/p>\r\n<p>The Department may in the future issue\r\n  regulations or propose modifications to Part 14 of the Banking Board\u0092s\r\n  regulations specifically to cover investments in subsidiaries by FBOs.\r\n  However, until further notice, this letter and Part 14 should be used as\r\n  general guidance for FBOs\u0092 investments in operating subsidiaries. Please\r\n  direct any questions concerning this guidance to the Department\u0092s Legal\r\n  Division at (212) 618-6591.<\/p>\r\n<p>Very truly yours,<\/p>\r\n<p>Elizabeth McCaul<br>\r\n  Superintendent of Banks<\/p>\r\n<!-- InstanceEndEditable -->\r\n          <\/div>\r\n\t\t\r\n\t \r\n       \r\n       <\/div>","format":"full_html"}],"field_dfs_page_structure":[{"target_id":"51","target_type":"taxonomy_term"}]}'

 */