<?php

/**
 * @file
 * Settings for crawl_parse_dfs_post_drupal.php
 * Author: Gung Wang
 * Date: September 2019
 */

define('LOCAL_DIR', '/Sites/local_dfs');
define('IMAGE_URL', '/sites/default/files/dfs_images');
define('FILE_URL', '/docs');
define('PREFIX', ''); // "//div[@id='content']"
define("CONTENE_TYPE", 'dfs_page');
define('FIELD_TAX', 'field_dfs_page_structure');

// Check the site is HTTP or HTTPS.
define('HTTP', 'http');
// define('HTTP', 'https');

define('JSON_DIR', '/Sites/migration/output/json/');

$summary_urls_out = '/Sites/migration/output/url-has-urls.json';

// Customize the Query for DOMXPath object;
$queries = [
  PREFIX . "//div[@id='homepub']",
  PREFIX . "//div[@id='procright']",
  PREFIX . "//div[@id='rightcol']",
  PREFIX . "//div[@class='pub']",
];
// $queries = [
//   PREFIX."//div[@id='columns']",
// ];


$terms_map = [
  '/consumer/student_protection' => '66', // Consumers - Student Protection
  '/consumer/shopping_sheet' => '66', // Consumersm- Student Protection
  '/consumer/auto' => '61', // Consumers - Auto Insurance
  '/consumer/homeown' => '71', // Consumers - Help for Homeowners
  '/consumer/inshelp' => '76', // Consumers - Insurance Help
  '/consumer/ltc' => '81', // Consumers - Long Term Care
  '/consumer/healthyny' => '86', // Consumers - Medicare Beneficiaries
  '/consumer/holocaust' => '91', // Consumers - Holocaust Claims
  '/healthyny/rates' => '134', // Consumers - Healthy New York Rates by County
  
  '/insurance/life' => '106', // Applications & Licensing - Life Insurances
  '/insurance/health' => '111', // Applications & Licensing - Health Insurers
  '/insurance/ogco*' => '133', // Applications & Licensing - Office of General Counsel Opinion
  '/insurance/circltr' => '135', // Applications & Licensing - Circular Letters Issued
  
  '/legal/interpret' => '131', // Industry Guidence - Banking Interpretations
  '/legal/industry' => '132', // Industry Guidence - Legal industry
  '/reportpub/wb' => '116', // Reports & Publications - Weekly Bulletins
  
  '/about/press' => '121',  // Contact Us - Press Release
  '/about/statements' => '126',  // Contact Us - Superintendent Statements

  '/consumer' => '36', // Consumers
  '/banking' => '96', // Applications & Licensing - Banking
  '/insurance' => '101', // Applications & Licensing - Insurance Companies
  '/legal' => '46', // Industry Guidence
  '/reportpub' => '51', // Reports & Publications
  '/about' => '56',  // Contact Us
  'default' => '56',
  ];


$nav_menu =  array(
    // menus: top & 2nd level..
    '/index.html',
    '/about/dfs_about.htm',
    '/consumer/dfs_consumers.htm',
    '/banking/dfs_banking.htm',
    '/insurance/dfs_insurance.htm',
    '/legal/dfs_legal.htm',
    '/reportpub/dfs_reportpub.htm',
    '/appliclicen.htm',

    '/about/whowesupervise.htm',
    '/about/news.htm',
    '/about/contactus.htm',
    '/about/mission.htm',
    '/about/dfs_initiatives.htm',
    '/about/history.htm',
    '/about/whowesupervise.htm',
    '/about/careerdfs.htm',
    '/about/procure.htm',

    '/consumer/fileacomplaint.htm',
    '/consumer/scamsfraud.htm',
    '/consumer/mortg.htm',
    '/consumer/consindx.htm',
    '/consumer/banksav.htm',
    '/consumer/creditdebt.htm',
    '/consumer/holocaust/hcpoindex.htm',

    '/banking/banktrust.htm',
    '/banking/mortgagecomp.htm',
    '/banking/ialf.htm',
    '/banking/billassess.htm',
    '/legal/industrylet.htm',
    '/banking/crabdd.htm',

    '/insurance/abindx.htm',
    '/insurance/iindx.htm',
    '/insurance/iprop.htm',
    '/insurance/ilife.htm',
    '/insurance/ihealth.htm',
    '/insurance/tocol4.htm',

    '/legal/regulations.htm',
    '/legal/opinion.htm',
    '/legal/industrylet.htm',
    '/legal/interagency_agree_mou.htm',
    '/legal/foil.htm',
    '/legal/leg_summ/leg_summ.htm',

    '/reportpub/examrep.htm',
    '/reportpub/cra_reports/crarate.htm',
    '/reportpub/fraudrep.htm',
    '/reportpub/wb.htm',
    '/reportpub/commentltr.htm',
    '/reportpub/annualrep.htm',

    // footer..
    '/consumer/fileacomplaint.htm',
    '/consumer/auto/obtain_lien_release.htm',
    '/consumer/tenantrights.htm',
    '/consumer/scamsfraud.htm',
    '/legal/foil.htm',
    '/insurance/extapp/extappqa.htm',
    '/insurance/dmvindex.htm',

    '/insurance/licensing/prolic.htm',
    '/insurance/faqs/faqs_serv_pro_conind.htm',
    '/banking/pff.htm',
    '/legal/industry/ilnameapro.htm',
    '/legal/industry/ilnameapro.htm',
    '/insurance/fdidx_in.htm',
    '/insurance/licensing/permit_temp_adjuster.htm',

    '/accessibility.htm',
    '/disclaimer.htm',
    '/privacy.htm',
    '/sitemap.htm',
    '/shift/adobe_reader.htm',

    '/about/laa_about.htm',
  );
